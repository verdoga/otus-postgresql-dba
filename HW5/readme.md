# Домашнее задание 5

## Вводная

Использовалась виртуальная машина VMware:
* 2 core
* 4 ram
* SSD 60GB
* Ubuntu 22.04
* pg14

## Этап 1

Конфигурацию СУБД изменил, как предложено было в файле к занятию.
* max_connections = 40
* shared_buffers = 1GB
* effective_cache_size = 3GB
* maintenance_work_mem = 512MB
* checkpoint_completion_target = 0.9
* wal_buffers = 16MB
* default_statistics_target = 500
* random_page_cost = 4
* effective_io_concurrency = 2
* work_mem = 6553kB
* min_wal_size = 4GB
* max_wal_size = 16GB

![Результат](https://gitlab.com/verdoga/otus-postgresql-dba/-/blob/main/HW5/1.png)

Предварительное тестирование. По моим ощущениям autovacuum так и не сработал, уж больно гладко всё было. Мелкие отклонения возникают из-за хоста.

![Результат2](https://gitlab.com/verdoga/otus-postgresql-dba/-/blob/main/HW5/2.png)

## Этап 2

Поигравшись с параметрами, я пришёл к выводу, что в моём случае работа с autovacuum всё равно довольно не равномерна. Идея состояла в том, чтобы заставить его работать регулярно мелкими сессиями (у нас нагрузка pgbench постоянна и довольно равномерна), но при этом давать роздых системе по максимальному, то есть на 100ms (сначала выставил 200ms, но кластер отказался подниматься - максимум 100ms).

Параметры:
![Результат3](https://gitlab.com/verdoga/otus-postgresql-dba/-/blob/main/HW5/3.png)

Результат тестирования получился менее гладким, но я в это время работал на хосте, отсюда неравномерность. Но наглядно видно, что autovacuum работал постоянно, а значит и серьёзных снижений производительности в перспективе при таком типе нагрузки не должно быть.

![Результат4](https://gitlab.com/verdoga/otus-postgresql-dba/-/blob/main/HW5/4.png)
