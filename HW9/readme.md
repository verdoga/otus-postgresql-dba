# Домашнее задание №9

## Вводная 

4 VM Vmware, Ubuntu 22.04 server, PG14.

## Этап 1

На каждой машине был настроен свой pg_hba.conf. На 1 и 2 машинах был выставлен wal_level = logical. Созданы перекрёстные подписки.

Слева - 1 VM, справа - 2 VM.

![Результат](https://gitlab.com/verdoga/otus-postgresql-dba/-/blob/main/HW9/1.png)

## Этап 2

Создаём 3 и 4 VM, на 3 подписываемся на первую и вторую. Wal_level = replica на третьей и четвёртой машине.

На четвёртой настраиваем горячую реплику, не логическую при помощи pg_basebackup.

Слева - 3 VM, Справа - 4 VM. Записи были добавлены на 1 и 2 машинах соответственно.

![Результат2](https://gitlab.com/verdoga/otus-postgresql-dba/-/blob/main/HW9/2.png)
