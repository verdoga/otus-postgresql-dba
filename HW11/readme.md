
# Домашнее задание №11

## Вводная

VMware VM, 2 vCPU, 4 GB vRAM, PG14, Ubuntu 22.04 server, база flights_big.

## Этап 1

Тестируем производительность запросом на выборку:

```sql
SELECT * FROM bookings.bookings_range WHERE book_date = '2016-07-04';
```

У меня таблица большая на 2,1 млн строк. Периоды с сентября 2015, по октябрь 2016.

Результат выполнения:
```json
[
  {
    "Plan": {
      "Node Type": "Gather",
      "Parallel Aware": false,
      "Async Capable": false,
      "Startup Cost": 1000.00,
      "Total Cost": 25442.86,
      "Plan Rows": 5,
      "Plan Width": 21,
      "Actual Rows": 6,
      "Actual Loops": 1,
      "Workers Planned": 2,
      "Workers Launched": 2,
      "Single Copy": false,
      "Plans": [
        {
          "Node Type": "Seq Scan",
          "Parent Relationship": "Outer",
          "Parallel Aware": true,
          "Async Capable": false,
          "Relation Name": "bookings",
          "Alias": "bookings",
          "Startup Cost": 0.00,
          "Total Cost": 24442.36,
          "Plan Rows": 2,
          "Plan Width": 21,
          "Actual Rows": 2,
          "Actual Loops": 3,
          "Filter": "(book_date = '2016-07-04 00:00:00+00'::timestamp with time zone)",
          "Rows Removed by Filter": 703701,
          "Workers": [
          ]
        }
      ]
    },
    "Triggers": [
    ]
  }
]
```

## Этап 2

Создаём таблицу и секции:

```sql
CREATE TABLE bookings.bookings_range (
       book_ref     character(6),
       book_date    timestamptz,
       total_amount numeric(10,2)
   ) PARTITION BY RANGE (book_date);

CREATE TABLE bookings.bookings_range_201509 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2015-09-01'::timestamptz) TO ('2015-10-01'::timestamptz);

CREATE TABLE bookings.bookings_range_201510 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2015-10-01'::timestamptz) TO ('2015-11-01'::timestamptz);

CREATE TABLE bookings.bookings_range_201511 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2015-11-01'::timestamptz) TO ('2015-12-01'::timestamptz);

CREATE TABLE bookings.bookings_range_201512 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2015-12-01'::timestamptz) TO ('2016-01-01'::timestamptz);

CREATE TABLE bookings.bookings_range_201601 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2016-01-01'::timestamptz) TO ('2016-02-01'::timestamptz);

CREATE TABLE bookings.bookings_range_201602 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2016-02-01'::timestamptz) TO ('2016-03-01'::timestamptz);

CREATE TABLE bookings.bookings_range_201603 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2016-03-01'::timestamptz) TO ('2016-04-01'::timestamptz);
	   
CREATE TABLE bookings.bookings_range_201604 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2016-04-01'::timestamptz) TO ('2016-05-01'::timestamptz);
	   
CREATE TABLE bookings.bookings_range_201605 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2016-05-01'::timestamptz) TO ('2016-06-01'::timestamptz);
	   
CREATE TABLE bookings.bookings_range_201606 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2016-06-01'::timestamptz) TO ('2016-07-01'::timestamptz);
	   
CREATE TABLE bookings.bookings_range_201607 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2016-07-01'::timestamptz) TO ('2016-08-01'::timestamptz);
	   
CREATE TABLE bookings.bookings_range_201608 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2016-08-01'::timestamptz) TO ('2016-09-01'::timestamptz);
	   
CREATE TABLE bookings.bookings_range_201609 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2016-09-01'::timestamptz) TO ('2016-10-01'::timestamptz);
	   
CREATE TABLE bookings.bookings_range_201610 PARTITION OF bookings.bookings_range 
       FOR VALUES FROM ('2016-10-01'::timestamptz) TO ('2016-11-01'::timestamptz);
```

Выполняем:

```sql
SELECT * FROM bookings.bookings_range WHERE book_date = '2016-07-04';
```

```json
[
  {
    "Plan": {
      "Node Type": "Seq Scan",
      "Parallel Aware": false,
      "Async Capable": false,
      "Relation Name": "bookings_range_201607",
      "Alias": "bookings_range",
      "Startup Cost": 0.00,
      "Total Cost": 3225.95,
      "Plan Rows": 5,
      "Plan Width": 21,
      "Actual Rows": 6,
      "Actual Loops": 1,
      "Filter": "(book_date = '2016-07-04 00:00:00+00'::timestamp with time zone)",
      "Rows Removed by Filter": 170950
    },
    "Triggers": [
    ]
  }
]
```

Ускорение ощутимое.
