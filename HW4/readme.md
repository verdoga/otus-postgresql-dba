# домашнее задание 4

## Вводная

Использовалась VM на базе VMware с Ubuntu и Postgres в контейнере.

## Этап 1

Создаём БД, схему, таблицу. Я сразу указал схему при создании таблицы testnm.test, поскольку знаю этот нюанс с public. Сталкивался с ним ранее.

Создал роль, пользователя и выдал привелегии на использование.

![Результат](https://gitlab.com/verdoga/otus-postgresql-dba/-/blob/main/HW4/1.png)

## Этап 2

По запросу 

```sql
SELECT * FROM test;
```

Не находит в нашей схеме testnm. Revoke не помогает откатить, удаление схемы тоже. После зачистки найдено стабильное решение через:

```sql 
ALTER DATABASE testdb SET search_path TO '$user', 'testnm';
```

Тогда всё работает нормально и ожидаемо. Но это не отменяет действий по уборке схемы public.

![Результат2](https://gitlab.com/verdoga/otus-postgresql-dba/-/blob/main/HW4/2.png)
