# Домашнее задание №10

## Вводная

VMware VM, 4 GB, 2 vCPU, Ubuntu 22.04 server, PG14, демонстрационная БД Flights.

## Этап 1 - обычный индекс btree

```sql
select passenger_id, passenger_name from bookings.tickets where passenger_id = '4030 855525';
```

Результат без индекса:

```json
[
  {
    "Plan": {
      "Node Type": "Gather",
      "Parallel Aware": false,
      "Async Capable": false,
      "Startup Cost": 1000.00,
      "Total Cost": 65778.94,
      "Plan Rows": 1,
      "Plan Width": 28,
      "Actual Startup Time": 0.427,
      "Actual Total Time": 176.889,
      "Actual Rows": 1,
      "Actual Loops": 1,
      "Output": ["passenger_id", "passenger_name"],
      "Workers Planned": 2,
      "Workers Launched": 2,
      "Single Copy": false,
      "Shared Hit Blocks": 225,
      "Shared Read Blocks": 49190,
      "Shared Dirtied Blocks": 0,
      "Shared Written Blocks": 0,
      "Local Hit Blocks": 0,
      "Local Read Blocks": 0,
      "Local Dirtied Blocks": 0,
      "Local Written Blocks": 0,
      "Temp Read Blocks": 0,
      "Temp Written Blocks": 0,
      "Plans": [
        {
          "Node Type": "Seq Scan",
          "Parent Relationship": "Outer",
          "Parallel Aware": true,
          "Async Capable": false,
          "Relation Name": "tickets",
          "Schema": "bookings",
          "Alias": "tickets",
          "Startup Cost": 0.00,
          "Total Cost": 64778.84,
          "Plan Rows": 1,
          "Plan Width": 28,
          "Actual Startup Time": 111.550,
          "Actual Total Time": 169.677,
          "Actual Rows": 0,
          "Actual Loops": 3,
          "Output": ["passenger_id", "passenger_name"],
          "Filter": "((tickets.passenger_id)::text = '4030 855525'::text)",
          "Rows Removed by Filter": 983285,
          "Shared Hit Blocks": 225,
          "Shared Read Blocks": 49190,
          "Shared Dirtied Blocks": 0,
          "Shared Written Blocks": 0,
          "Local Hit Blocks": 0,
          "Local Read Blocks": 0,
          "Local Dirtied Blocks": 0,
          "Local Written Blocks": 0,
          "Temp Read Blocks": 0,
          "Temp Written Blocks": 0,
          "Workers": [
            {
              "Worker Number": 0,
              "Actual Startup Time": 163.454,
              "Actual Total Time": 163.454,
              "Actual Rows": 0,
              "Actual Loops": 1,
              "Shared Hit Blocks": 109,
              "Shared Read Blocks": 17488,
              "Shared Dirtied Blocks": 0,
              "Shared Written Blocks": 0,
              "Local Hit Blocks": 0,
              "Local Read Blocks": 0,
              "Local Dirtied Blocks": 0,
              "Local Written Blocks": 0,
              "Temp Read Blocks": 0,
              "Temp Written Blocks": 0
            },
            {
              "Worker Number": 1,
              "Actual Startup Time": 171.190,
              "Actual Total Time": 171.191,
              "Actual Rows": 0,
              "Actual Loops": 1,
              "Shared Hit Blocks": 0,
              "Shared Read Blocks": 14520,
              "Shared Dirtied Blocks": 0,
              "Shared Written Blocks": 0,
              "Local Hit Blocks": 0,
              "Local Read Blocks": 0,
              "Local Dirtied Blocks": 0,
              "Local Written Blocks": 0,
              "Temp Read Blocks": 0,
              "Temp Written Blocks": 0
            }
          ]
        }
      ]
    },
    "Settings": {
    },
    "Planning": {
      "Shared Hit Blocks": 4,
      "Shared Read Blocks": 0,
      "Shared Dirtied Blocks": 0,
      "Shared Written Blocks": 0,
      "Local Hit Blocks": 0,
      "Local Read Blocks": 0,
      "Local Dirtied Blocks": 0,
      "Local Written Blocks": 0,
      "Temp Read Blocks": 0,
      "Temp Written Blocks": 0
    },
    "Planning Time": 0.055,
    "Triggers": [
    ],
    "Execution Time": 176.901
  }
]
```

С индексом:

```json
[
  {
    "Plan": {
      "Node Type": "Index Scan",
      "Parallel Aware": false,
      "Async Capable": false,
      "Scan Direction": "Forward",
      "Index Name": "idx_passenger_id",
      "Relation Name": "tickets",
      "Schema": "bookings",
      "Alias": "tickets",
      "Startup Cost": 0.43,
      "Total Cost": 8.45,
      "Plan Rows": 1,
      "Plan Width": 28,
      "Actual Startup Time": 0.013,
      "Actual Total Time": 0.014,
      "Actual Rows": 1,
      "Actual Loops": 1,
      "Output": ["passenger_id", "passenger_name"],
      "Index Cond": "((tickets.passenger_id)::text = '4030 855525'::text)",
      "Rows Removed by Index Recheck": 0,
      "Shared Hit Blocks": 4,
      "Shared Read Blocks": 0,
      "Shared Dirtied Blocks": 0,
      "Shared Written Blocks": 0,
      "Local Hit Blocks": 0,
      "Local Read Blocks": 0,
      "Local Dirtied Blocks": 0,
      "Local Written Blocks": 0,
      "Temp Read Blocks": 0,
      "Temp Written Blocks": 0
    },
    "Settings": {
    },
    "Planning": {
      "Shared Hit Blocks": 0,
      "Shared Read Blocks": 0,
      "Shared Dirtied Blocks": 0,
      "Shared Written Blocks": 0,
      "Local Hit Blocks": 0,
      "Local Read Blocks": 0,
      "Local Dirtied Blocks": 0,
      "Local Written Blocks": 0,
      "Temp Read Blocks": 0,
      "Temp Written Blocks": 0
    },
    "Planning Time": 0.043,
    "Triggers": [
    ],
    "Execution Time": 0.024
  }
]
```

## Этап 2 - Полнотекстовый GIN

```sql
select passenger_name, contact_data from bookings.tickets where contact_data @> '{"phone": "+70110137563"}'::jsonb;
```

С индексом:

```json
[
  {
    "Plan": {
      "Node Type": "Bitmap Heap Scan",
      "Parallel Aware": false,
      "Async Capable": false,
      "Relation Name": "tickets",
      "Alias": "tickets",
      "Startup Cost": 46.29,
      "Total Cost": 1161.59,
      "Plan Rows": 295,
      "Plan Width": 71,
      "Actual Rows": 1,
      "Actual Loops": 1,
      "Recheck Cond": "(contact_data @> '{\"phone\": \"+70110137563\"}'::jsonb)",
      "Rows Removed by Index Recheck": 0,
      "Exact Heap Blocks": 1,
      "Lossy Heap Blocks": 0,
      "Plans": [
        {
          "Node Type": "Bitmap Index Scan",
          "Parent Relationship": "Outer",
          "Parallel Aware": false,
          "Async Capable": false,
          "Index Name": "idx_contact_data",
          "Startup Cost": 0.00,
          "Total Cost": 46.21,
          "Plan Rows": 295,
          "Plan Width": 0,
          "Actual Rows": 1,
          "Actual Loops": 1,
          "Index Cond": "(contact_data @> '{\"phone\": \"+70110137563\"}'::jsonb)"
        }
      ]
    },
    "Planning Time": 0.061,
    "Triggers": [
    ],
    "Execution Time": 0.047
  }
]
```

Без индекса:

```json
[
  {
    "Plan": {
      "Node Type": "Gather",
      "Parallel Aware": false,
      "Async Capable": false,
      "Startup Cost": 1000.00,
      "Total Cost": 65808.34,
      "Plan Rows": 295,
      "Plan Width": 71,
      "Actual Rows": 1,
      "Actual Loops": 1,
      "Workers Planned": 2,
      "Workers Launched": 2,
      "Single Copy": false,
      "Plans": [
        {
          "Node Type": "Seq Scan",
          "Parent Relationship": "Outer",
          "Parallel Aware": true,
          "Async Capable": false,
          "Relation Name": "tickets",
          "Alias": "tickets",
          "Startup Cost": 0.00,
          "Total Cost": 64778.84,
          "Plan Rows": 123,
          "Plan Width": 71,
          "Actual Rows": 0,
          "Actual Loops": 3,
          "Filter": "(contact_data @> '{\"phone\": \"+70110137563\"}'::jsonb)",
          "Rows Removed by Filter": 983285,
          "Workers": [
          ]
        }
      ]
    },
    "Planning Time": 0.076,
    "Triggers": [
    ],
    "Execution Time": 338.724
  }
]
```

## Этап 3 - частичный индекс

Условие: passenger_name like '%MIKHAIL%';

```sql
select passenger_name from bookings.tickets where passenger_name like '%MIKHAIL%';
```

```json
[
  {
    "Plan": {
      "Node Type": "Index Only Scan",
      "Parallel Aware": false,
      "Async Capable": false,
      "Scan Direction": "Forward",
      "Index Name": "idx_passenger_name",
      "Relation Name": "tickets",
      "Alias": "tickets",
      "Startup Cost": 0.29,
      "Total Cost": 936.99,
      "Plan Rows": 56426,
      "Plan Width": 16,
      "Actual Rows": 42488,
      "Actual Loops": 1,
      "Heap Fetches": 0
    },
    "Planning Time": 0.125,
    "Triggers": [
    ],
    "Execution Time": 1.872
  }
]
```

Без индекса:

```json
[
  {
    "Plan": {
      "Node Type": "Gather",
      "Parallel Aware": false,
      "Async Capable": false,
      "Startup Cost": 1000.00,
      "Total Cost": 71421.44,
      "Plan Rows": 56426,
      "Plan Width": 16,
      "Actual Rows": 42488,
      "Actual Loops": 1,
      "Workers Planned": 2,
      "Workers Launched": 2,
      "Single Copy": false,
      "Plans": [
        {
          "Node Type": "Seq Scan",
          "Parent Relationship": "Outer",
          "Parallel Aware": true,
          "Async Capable": false,
          "Relation Name": "tickets",
          "Alias": "tickets",
          "Startup Cost": 0.00,
          "Total Cost": 64778.84,
          "Plan Rows": 23511,
          "Plan Width": 16,
          "Actual Rows": 14163,
          "Actual Loops": 3,
          "Filter": "(passenger_name ~~ '%MIKHAIL%'::text)",
          "Rows Removed by Filter": 969123,
          "Workers": [
          ]
        }
      ]
    },
    "Planning Time": 0.080,
    "Triggers": [
    ],
    "Execution Time": 317.388
  }
]
```

## Этап 4 - индекс на несколько полей

Создание

```sql
CREATE INDEX idx
    ON bookings.tickets
    (passenger_id,
    passenger_name)
;
```

Запрос:

```sql
select passenger_name from bookings.tickets where passenger_name like '%MIKHAIL%' AND passenger_id like '%484%';
```

```json
[
  {
    "Plan": {
      "Node Type": "Gather",
      "Parallel Aware": false,
      "Async Capable": false,
      "Startup Cost": 1000.00,
      "Total Cost": 68852.21,
      "Plan Rows": 6,
      "Plan Width": 16,
      "Actual Rows": 256,
      "Actual Loops": 1,
      "Workers Planned": 2,
      "Workers Launched": 2,
      "Single Copy": false,
      "Plans": [
        {
          "Node Type": "Seq Scan",
          "Parent Relationship": "Outer",
          "Parallel Aware": true,
          "Async Capable": false,
          "Relation Name": "tickets",
          "Alias": "tickets",
          "Startup Cost": 0.00,
          "Total Cost": 67851.61,
          "Plan Rows": 2,
          "Plan Width": 16,
          "Actual Rows": 85,
          "Actual Loops": 3,
          "Filter": "((passenger_name ~~ '%MIKHAIL%'::text) AND ((passenger_id)::text ~~ '%484%'::text))",
          "Rows Removed by Filter": 983200,
          "Workers": [
          ]
        }
      ]
    },
    "Planning Time": 0.165,
    "Triggers": [
    ],
    "Execution Time": 245.145
  }
]
```

Результат с индексом и без него одинаковый. Я так и не смог разобраться в чём причина игнорирования индекса планировщиком запросов, увы.
