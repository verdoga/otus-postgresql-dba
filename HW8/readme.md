# Домашнее задание №8

## Вводная

* Ubuntu 22.04
* VMware vm
* PG14
* 2vCPU
* 4 vRAM
* 60GB SSD SATA controller.

# Этап 1

Настраиваем:
* max_connections = 20 - 20 использовалось и при тестировании
* shared_buffers = 1GB - рекомендовано pgtune
* effective_cache_size = 3GB - рекомендовано pgtune
* maintenance_work_mem = 256MB - рекомендовано pgtune
* checkpoint_completion_target = 0.9 - рекомендовано pgtune
* wal_buffers = 16MB - рекомендовано pgtune
* default_statistics_target = 200 - сделал так, чтобы планировщик мог более эффективно расходовать память и оптимизировать работу
* random_page_cost = 1.1 - рекомендовано pgtune
* effective_io_concurrency = 500 - у меня NVMe SSD. Хотя в VM подключён как SATA.
* work_mem = 26214kB - рекомендовано pgtune
* min_wal_size = 2GB - рекомендовано pgtune
* max_wal_size = 8GB - рекомендовано pgtune
* checkpoint_timeout = 60s - распределяем нагрузку на запись более мелкими порциями, чтобы не накапливалось (предсказуемый уровень производительности)
* synchronous_commit = off - позволяет хорошо поднять производительность, мы не банк, можно:)
* wal_level = minimal - отрубаем репликацию, пока не надо.
* fsync = off снижаем зависимость от медленных накопителей
* autovacuum = on - вакуум настроен, потому что он в любом случае должен использоваться, замеры без него не совсем честные.
* autovacuum_max_workers = 2
* autovacuum_vacuum_threshold = 20
* autovacuum_analyze_threshold = 20
* autovacuum_vacuum_scale_factor = 0.01
* autovacuum_analyze_scale_factor = 0.01
* autovacuum_cost_delay = 20ms
* autovacuum_vacuum_cost_limit = 200

Результат средний TPS для 20 клиентов 2364 и это на двух ядрах в виртуалке под форточкой.

![Результат](https://gitlab.com/verdoga/otus-postgresql-dba/-/blob/main/HW8/1.png)

